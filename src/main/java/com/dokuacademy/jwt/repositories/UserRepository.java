package com.dokuacademy.jwt.repositories;

import com.dokuacademy.jwt.dto.CreateUserResponse;
import com.dokuacademy.jwt.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
   Optional<User> findByEmail(String email);
}
