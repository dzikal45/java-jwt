package com.dokuacademy.jwt.services;

import com.dokuacademy.jwt.dto.CreateUserRequest;
import com.dokuacademy.jwt.dto.CreateUserResponse;

public interface IUserService {
    CreateUserResponse create(CreateUserRequest req);
    CreateUserResponse findByEmail(String Email);

}
