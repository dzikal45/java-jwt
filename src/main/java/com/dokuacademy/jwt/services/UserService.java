package com.dokuacademy.jwt.services;

import com.dokuacademy.jwt.dto.CreateUserRequest;
import com.dokuacademy.jwt.dto.CreateUserResponse;
import com.dokuacademy.jwt.entities.User;
import com.dokuacademy.jwt.repositories.UserRepository;
import lombok.Getter;
import lombok.Setter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
@Setter
@Getter
public class UserService implements IUserService{
    @Autowired
    UserRepository userRepository;
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    PasswordEncoder passwordEncoder;

    private User toEntity(CreateUserRequest req){
        return modelMapper.map(req,User.class);
    }
    private CreateUserResponse toDto(User user){
        return modelMapper.map(user, CreateUserResponse.class);
    }


    @Override
    public CreateUserResponse create(CreateUserRequest req) {
        User user = toEntity(req);
        Optional<User> userOptional = userRepository.findByEmail(user.getEmail());
        if(userOptional.isPresent()){
            throw new  ResponseStatusException(HttpStatus.CONFLICT,"User alredy exist");
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        User res  = userRepository.save(user);
        return toDto(res);


    }

    @Override
    public CreateUserResponse findByEmail(String Email) {
        User userOptional  = userRepository.findByEmail(Email).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
        return toDto(userOptional);

    }
}
