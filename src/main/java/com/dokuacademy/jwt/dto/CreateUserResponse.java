package com.dokuacademy.jwt.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CreateUserResponse {
    private Long id;
    private String email;
}
