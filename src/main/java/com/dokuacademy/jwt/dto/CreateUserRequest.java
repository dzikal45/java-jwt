package com.dokuacademy.jwt.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CreateUserRequest {
    @JsonProperty(required = true)
    private Long id;
    @JsonProperty(required = true)
    private String email;
    @JsonProperty(required = true)
    private String password;
}
