package com.dokuacademy.jwt.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
@Setter
@Getter
public class BaseResponse <T>{
    private HttpStatus code;
    private String message;
    private T data;

    public BaseResponse(HttpStatus code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }
}
