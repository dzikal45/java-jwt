package com.dokuacademy.jwt.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "users")
@Setter
@Getter
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String email;
    private String password;
    protected User(){};

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
