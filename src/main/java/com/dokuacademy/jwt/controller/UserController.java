package com.dokuacademy.jwt.controller;

import com.dokuacademy.jwt.dto.BaseResponse;
import com.dokuacademy.jwt.dto.CreateUserRequest;
import com.dokuacademy.jwt.dto.CreateUserResponse;
import com.dokuacademy.jwt.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
public class UserController {
    @Autowired
    IUserService userService;
    @PostMapping("/auth/register")
    public BaseResponse create(@RequestBody CreateUserRequest req){
        CreateUserResponse res = userService.create(req);
        return new BaseResponse(HttpStatus.ACCEPTED,"success",res);
    }
    @GetMapping("/user")
    public BaseResponse getUser(Authentication authentication){
        CreateUserResponse res = userService.findByEmail(authentication.getName());
        return new BaseResponse(HttpStatus.ACCEPTED,"success",res);
    }
}
